<?php

namespace Drupal\tlsrpt\Plugin\views\wizard;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsWizard;
use Drupal\views\Plugin\views\wizard\WizardPluginBase;

/**
 * Creates TLSRPT views.
 */
#[ViewsWizard(
  id: 'tlsrpt',
  base_table: 'tlsrpt',
  title: new TranslatableMarkup('TLSRPT')
)]
class Tlsrpt extends WizardPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $createdColumn = 'received';

}
