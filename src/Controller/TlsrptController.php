<?php

namespace Drupal\tlsrpt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\tlsrpt\Entity\Tlsrpt;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for TLSRPT routes.
 */
class TlsrptController extends ControllerBase {

  /**
   * Maximum acceptable length of submitted TLSRPT in bytes.
   */
  const LIMIT = 10485760;

  /**
   * Set zlib.inflate window to maximize compatibility.
   */
  const PARAMS = ['window' => 47];

  /**
   * Rate limit threshold.
   */
  const THRESHOLD = 10;

  /**
   * Rate limit window.
   */
  const WINDOW = 3600;

  /**
   * The controller constructor.
   */
  final public function __construct(
    protected FloodInterface $flood,
    protected MailManagerInterface $mailManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('flood'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * Extracts TLSRPT from request, saves it and logs any failures.
   */
  public function receive(Request $request): Response {
    if (!$this->flood->isAllowed('tlsrpt', static::THRESHOLD, static::WINDOW)) {
      $this->getLogger('tlsrpt')->notice('TLSRPT submission rejected by rate limiting.');
      return new Response($this->t('NAK') . PHP_EOL, Response::HTTP_TOO_MANY_REQUESTS, ['Retry-After' => static::WINDOW]);
    }
    $stream = $request->getContent(TRUE);
    $type = $request->headers->get('Content-Type');
    if ('application/tlsrpt+gzip' === $type) {
      stream_filter_append($stream, 'zlib.inflate', STREAM_FILTER_READ, static::PARAMS);
    }
    elseif ('application/tlsrpt+json' !== $type) {
      $this->getLogger('tlsrpt')->notice('TLSRPT submission unexpected media type.');
      return new Response($this->t('NAK') . PHP_EOL, Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
    }
    $json = stream_get_contents($stream, static::LIMIT);
    if (!feof($stream)) {
      $this->getLogger('tlsrpt')->notice('TLSRPT submission exceeded size limit.');
      return new Response($this->t('NAK') . PHP_EOL, Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
    }
    // Not sure FALSE is possible here, but just in case.
    if (FALSE === $json) {
      $this->getLogger('tlsrpt')->notice('TLSRPT submission read failure.');
      return new Response($this->t('NAK') . PHP_EOL, Response::HTTP_BAD_REQUEST);
    }
    try {
      $input = json_decode($json, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    catch (\JsonException $e) {
      $this->getLogger('tlsrpt')->error('TLSRPT submission JSON parse error.', ['exception' => $e]);
      return new Response($this->t('NAK') . PHP_EOL, Response::HTTP_BAD_REQUEST);
    }
    if (!is_array($input)) {
      $this->getLogger('tlsrpt')->notice('TLSRPT submission is not an array.');
      return new Response($this->t('NAK') . PHP_EOL, Response::HTTP_BAD_REQUEST);
    }
    $this->process($input, $json);
    return new Response($this->t('ACK') . PHP_EOL, Response::HTTP_OK);
  }

  /**
   * Processes and logs a TLSRPT submission.
   *
   * @param mixed[] $input
   *   The TLSRPT submission extracted to an associative array.
   * @param string $json
   *   The TLSRPT submission as received in JSON format.
   */
  public function process(array $input, string $json): int {
    $failure = $successful = 0;
    if (is_array($input['policies'])) {
      foreach ($input['policies'] as $policy) {
        $failure += $policy['summary']['total-failure-session-count'];
        $successful += $policy['summary']['total-successful-session-count'];
      }
    }
    $tlsrpt = Tlsrpt::create([
      'report_id' => $input['report-id'] ?? NULL,
      'organization_name' => $input['organization-name'] ?? NULL,
      'contact_info' => $input['contact-info'] ?? NULL,
      'start_datetime' => strtotime($input['date-range']['start-datetime']) ?: NULL,
      'end_datetime' => strtotime($input['date-range']['end-datetime']) ?: NULL,
      'total_successful' => $successful,
      'total_failure' => $failure,
      'json_report' => $json,
    ]);
    $tlsrpt->save();
    $this->flood->register('tlsrpt', static::WINDOW);
    if ($failure) {
      $this->getLogger('tlsrpt')->error('SMTP TLS failure(s) reported.', ['link' => $tlsrpt->toLink()->toString()]);
      if (($mail = $this->config('tlsrpt.settings')->get('mail')) && is_string($mail)) {
        $this->mailManager->mail('tlsrpt', 'alert', $mail, $this->languageManager()->getCurrentLanguage()->getId(), ['entity' => $tlsrpt]);
      }
    }
    return (int) $tlsrpt->id();
  }

}
