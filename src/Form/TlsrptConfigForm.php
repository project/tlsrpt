<?php

namespace Drupal\tlsrpt\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements a SecureLogin Config form.
 */
class TlsrptConfigForm extends ConfigFormBase {

  /**
   * Constructs the TLSRPT config form.
   */
  final public function __construct(
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    protected DateFormatterInterface $dateFormatter,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('date.formatter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tlsrpt_config_form';
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   Editable config names.
   */
  protected function getEditableConfigNames() {
    return ['tlsrpt.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed[]
   *   Form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $intervals = [
      -1,
      86400 * 365,
      86400 * 270,
      86400 * 180,
      86400 * 120,
      86400 * 90,
      86400 * 60,
      86400 * 30,
      86400 * 28,
      86400 * 21,
      86400 * 14,
      86400 * 7,
      86400 * 6,
      86400 * 5,
      86400 * 4,
      86400 * 3,
      86400 * 2,
      86400 * 1,
    ];
    $options = array_combine($intervals, array_map([
      $this->dateFormatter,
      'formatInterval',
    ], $intervals));
    $options[-1] = $this->t('Preserve forever');
    $form['ttl'] = [
      '#type'          => 'select',
      '#title'         => $this->t('TLSRPT report TTL'),
      '#options'       => $options,
      '#config_target' => 'tlsrpt.settings:ttl',
      '#description'   => $this->t('Delete TLSRPT reports older than this interval.'),
    ];
    $form['mail'] = [
      '#type'          => 'email',
      '#title'         => $this->t('Email address'),
      '#config_target' => 'tlsrpt.settings:mail',
      '#description'   => $this->t('If specified, this email address will be alerted when failure reports are received.'),
    ];
    return parent::buildForm($form, $form_state);
  }

}
