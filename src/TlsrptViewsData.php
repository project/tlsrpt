<?php

namespace Drupal\tlsrpt;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the user entity type.
 */
class TlsrptViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Views data in the format of hook_views_data().
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();
    $data['tlsrpt']['table']['wizard_id'] = 'tlsrpt';
    return $data;
  }

}
