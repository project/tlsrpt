<?php

namespace Drupal\tlsrpt;

use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the node schema handler.
 */
class TlsrptStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   The schema definition for the table.
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();
    if ($table_name === 'tlsrpt' && $field_name === 'received') {
      $schema['fields'][$field_name]['not null'] = TRUE;
      $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
    }
    return $schema;
  }

}
