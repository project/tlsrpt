<?php

namespace Drupal\tlsrpt\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides HTML routes for entities with administrative pages.
 */
class TlsrptHtmlRouteProvider extends AdminHtmlRouteProvider {

}
