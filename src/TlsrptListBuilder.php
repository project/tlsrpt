<?php

namespace Drupal\tlsrpt;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the TLSRPT entity type.
 */
class TlsrptListBuilder extends EntityListBuilder {

  /**
   * Constructs a new TlsrptListBuilder object.
   */
  final public function __construct(
    EntityTypeInterface $entityType,
    EntityStorageInterface $storage,
    protected DateFormatterInterface $dateFormatter,
  ) {
    parent::__construct($entityType, $storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entityType): static {
    return new static(
      $entityType,
      $container->get('entity_type.manager')->getStorage($entityType->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Render array.
   */
  public function render() {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total TLSRPT reports received: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Entity IDs.
   */
  protected function getEntityIds() {
    $header = $this->buildHeader();
    $query = $this->getStorage()
      ->getQuery()
      ->accessCheck(TRUE)
      ->tableSort($header);
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Table header.
   */
  public function buildHeader() {
    $header['report_id'] = $this->t('Report ID');
    $header['organization_name'] = [
      'data' => $this->t('Organization name'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['total_successful'] = [
      'data' => $this->t('Total successful'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['total_failure'] = [
      'data' => $this->t('Total failures'),
    ];
    $header['received'] = [
      'data' => $this->t('Received'),
      'field' => 'received',
      'specifier' => 'received',
      'sort' => 'desc',
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header += parent::buildHeader();
    $header['operations'] = ['data' => $header['operations']];
    $header['operations']['class'] = [RESPONSIVE_PRIORITY_LOW];
    return $header;
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Table row.
   */
  public function buildRow(EntityInterface $entity) {
    /** @var TlsrptInterface $entity */
    $row['report_id']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $entity->toUrl(),
    ];
    $row['organization_name'] = $entity->get('organization_name')->value;
    $row['total_successful'] = $entity->get('total_successful')->value;
    $row['total_failure'] = $entity->get('total_failure')->value;
    $received = $entity->get('received')->value;
    $row['received'] = is_numeric($received) ? $this->dateFormatter->format((int) $received) : '';
    return $row + parent::buildRow($entity);
  }

}
