<?php

namespace Drupal\tlsrpt;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a TLSRPT entity type.
 */
interface TlsrptInterface extends ContentEntityInterface {

}
