<?php

namespace Drupal\tlsrpt\Entity;

use Drupal\Core\Entity\Attribute\ContentEntityType;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Form\DeleteMultipleForm;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\tlsrpt\Routing\TlsrptHtmlRouteProvider;
use Drupal\tlsrpt\TlsrptAccessControlHandler;
use Drupal\tlsrpt\TlsrptInterface;
use Drupal\tlsrpt\TlsrptListBuilder;
use Drupal\tlsrpt\TlsrptStorageSchema;
use Drupal\tlsrpt\TlsrptViewsData;

/**
 * Defines the TLSRPT entity class.
 */
#[ContentEntityType(
  id: 'tlsrpt',
  label: new TranslatableMarkup('TLSRPT'),
  label_collection: new TranslatableMarkup('TLSRPT'),
  label_singular: new TranslatableMarkup('TLSRPT'),
  label_plural: new TranslatableMarkup('TLSRPTs'),
  label_count: [
    'singular' => '@count TLSRPT',
    'plural' => '@count TLSRPTs',
  ],
  handlers: [
    'list_builder' => TlsrptListBuilder::class,
    'views_data' => TlsrptViewsData::class,
    'access' => TlsrptAccessControlHandler::class,
    'form' => [
      'delete' => ContentEntityDeleteForm::class,
      'delete-multiple-confirm' => DeleteMultipleForm::class,
    ],
    'route_provider' => [
      'html' => TlsrptHtmlRouteProvider::class,
    ],
    'storage_schema' => TlsrptStorageSchema::class,
  ],
  base_table: 'tlsrpt',
  admin_permission: 'administer tlsrpt',
  entity_keys: [
    'id' => 'id',
    'label' => 'report_id',
    'uuid' => 'uuid',
  ],
  links: [
    'collection' => '/admin/reports/tlsrpt',
    'canonical' => '/admin/reports/tlsrpt/{tlsrpt}',
    'delete-form' => '/admin/reports/tlsrpt/{tlsrpt}/delete',
    'delete-multiple-form' => '/admin/reports/tlsrpt/delete',
  ],
)]
class Tlsrpt extends ContentEntityBase implements TlsrptInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['report_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Report ID'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['organization_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Organization name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ]);

    $fields['contact_info'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Contact info'))
      ->setDescription(t('Contact email address.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'basic_string',
        'weight' => 5,
      ]);

    $fields['received'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Received on'))
      ->setDescription(t('The time that the TLSRPT was received.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 10,
      ]);

    $fields['start_datetime'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Start date/time'))
      ->setDescription(t('The start of the TLSRPT date range.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 15,
      ]);

    $fields['end_datetime'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('End date/time'))
      ->setDescription(t('The end of the TLSRPT date range.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ]);

    $fields['total_successful'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total successful'))
      ->setDescription(t('Total successful sessions across all policies in this report'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => 25,
      ]);

    $fields['total_failure'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total failure'))
      ->setDescription(t('Total failures across all policies in this report.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => 30,
      ]);

    $fields['json_report'] = BaseFieldDefinition::create('json_native_binary')
      ->setLabel(t('TLSRPT JSON'))
      ->setDescription(t('The TLSRPT object, serialized as JSON.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'json',
        'weight' => 35,
      ]);

    return $fields;
  }

}
