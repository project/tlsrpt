<?php

namespace Drupal\tlsrpt\Hook;

use Drupal\Core\Hook\Attribute\Hook;

/**
 * Implements hook_theme().
 */
#[Hook('theme')]
class Theme {

  /**
   * Implements hook_theme().
   *
   * @return array<string, array<string, string>>
   *   An associative array of theme hook information.
   */
  public function __invoke(): array {
    return [
      'tlsrpt' => [
        'render element' => 'elements',
      ],
    ];
  }

}
