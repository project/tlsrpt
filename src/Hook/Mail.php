<?php

namespace Drupal\tlsrpt\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Implements hook_mail().
 */
#[Hook('mail')]
class Mail {

  use StringTranslationTrait;

  public function __construct(protected LanguageManagerInterface $languageManager) {
  }

  /**
   * Implements hook_mail().
   *
   * @param string $key
   *   An identifier of the mail.
   * @param array{langcode: string} $message
   *   A message array to be filled in.
   * @param array{entity: \Drupal\tlsrpt\TlsrptInterface} $params
   *   An array of parameters supplied by the caller.
   */
  public function __invoke(string $key, array &$message, array $params): void {
    $options = [
      'absolute' => TRUE,
      'langcode' => $message['langcode'],
      'language' => $this->languageManager->getLanguage($message['langcode']),
    ];
    $message['subject'] = $this->t('SMTP TLS failure(s) reported.', [], $options);
    $message['body'][] = $this->t('An SMTP TLS failure has been reported via TLSRPT:', [], $options);
    $message['body'][] = $params['entity']->toUrl('canonical', $options)->toString();
    $message['body'][] = $this->t('This is an automated alert from:', [], $options);
    $message['body'][] = Url::fromRoute('tlsrpt.settings', [], $options)->toString();
  }

}
