<?php

namespace Drupal\tlsrpt\Hook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Hook\Attribute\Hook;

/**
 * Implements hook_cron().
 */
#[Hook('cron')]
class Cron {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected Connection $connection,
  ) {
  }

  /**
   * Implements hook_cron().
   */
  public function __invoke(): void {
    $ttl = $this->configFactory->get('tlsrpt.settings')->get('ttl');
    if ($ttl > 0) {
      $this->connection->delete('tlsrpt')
        ->condition('received', (string) (time() - $ttl), '<')
        ->execute();
    }
  }

}
