<?php

namespace Drupal\tlsrpt\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Implements hook_help().
 */
#[Hook('help')]
class Help {

  use StringTranslationTrait;

  /**
   * Implements hook_help().
   *
   * @return array<string, string|\Drupal\Core\StringTranslation\TranslatableMarkup>
   *   A render array containing the help text.
   */
  public function __invoke(?string $route_name): array {
    switch ($route_name) {
      case 'help.page.tlsrpt':
      case 'tlsrpt.settings':
      case 'entity.tlsrpt.collection':
        return [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('<a href="https://www.drupal.org/project/tlsrpt" rel="noreferrer">TLSRPT module</a> provides an endpoint for receiving <a href="https://datatracker.ietf.org/doc/html/rfc8460" rel="noreferrer">SMTP TLS reports</a> from participating MTAs.'),
        ];
    }
    return [];
  }

}
