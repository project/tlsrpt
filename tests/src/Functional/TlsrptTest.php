<?php

namespace Drupal\Tests\tlsrpt\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests TLSRPT module.
 *
 * @group tlsrpt
 */
class TlsrptTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tlsrpt'];

  /**
   * Functional tests for TLSRPT.
   */
  public function testTlsrpt(): void {
    $role = Role::load(Role::ANONYMOUS_ID);
    $this->assertNotNull($role);
    $role->grantPermission('create tlsrpt')->save();
    $this->getHttpClient()->request('POST', $this->buildUrl('v1/tlsrpt'), [
      'body' => file_get_contents(__DIR__ . '/../../fixtures/sample-data.json.gz'),
      'headers' => ['Content-Type' => 'application/tlsrpt+gzip'],
    ]);
    $this->assertSession()->responseContains('ACK');
    $admin = $this->drupalCreateUser(['administer tlsrpt']);
    $this->assertNotEmpty($admin);
    $this->drupalLogin($admin);
    $this->drupalGet('admin/reports/tlsrpt/1');
    $this->assertSession()->pageTextContains('Acme Corporation');
  }

}
