# TLSRPT (SMTP TLS Reporting)

This module allows you to receive SMTP TLS reports, as defined by RFC
8460, and log them to a database table.

TLSRPT reports can be received via HTTP POST at the /v1/tlsrpt path.

Make sure you give anonymous users permission to create TLSRPT entities,
otherwise access will be denied.

To start receiving TLSRPT reports from SMTP MTAs, add the following DNS
record on your domain (where your domain is example.org and your site
with TLSRPT module is at https://www.example.org):

```
_smtp._tls.example.org. IN TXT "v=TLSRPTv1; rua=https://www.example.org/v1/tlsrpt"
```

To mitigate abuse, TLSRPT reports are limited to 10 megabytes of JSON,
and 10 submissions per hour for each IP address.
